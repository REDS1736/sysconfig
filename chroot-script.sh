#! /bin/sh

# This script is called by `install.sh`

hostname="$1"
rootpw="$2"
nonrootusername="$3"
nonrootpw="$4"
gitmail="$5"
gitname="$6"
bootmode="$7"
setupraid="$8"
setupsoundcard="$9"
setupkeychron="${10}"
drivename="${11}"

echo "hostname: $hostname"
echo "rootpw: $rootpw"
echo "nonrootusername: $nonrootusername"
echo "nonrootpw: $nonrootpw"

echo "................................."
echo "KEYRING"
echo "................................."

pacman-key --populate archlinux || {
    echo "FAILED: pacman-key"; exit 1;
}


echo "................................."
echo "TIMEZONE / HARDWARE CLOCK"
echo "................................."

ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime || {
    echo "FAILED: ln ... /etc/localtime"; exit 1;
}
hwclock --systohc || {
    echo "FAILED: hwclock"; exit 1;
}


echo "................................."
echo "PREREQUISITES"
echo "................................."

pacman --noconfirm -S   neovim \
                        sudo \
                        grub \
                        networkmanager || {
    echo "FAILED: pacman"; exit 1;
}


echo "................................."
echo "LOCALE"
echo "................................."

# Uncomment line: en_US.UTF-8 UTF-8
sed -i '/en_US.UTF-8 UTF-8/s/^#//g' /etc/locale.gen
locale-gen || { echo "FAILED: locale-gen"; exit 1; }


echo "................................."
echo "HOSTNAME"
echo "................................."

echo "$hostname" > /etc/hostname

echo "127.0.0.1   localhost"                        >> /etc/hosts
echo "::1         localhost"                        >> /etc/hosts
echo "127.0.1.1   $hostname.localdomain $hostname"  >> /etc/hosts


echo "................................."
echo "USERS / PASSWORDS"
echo "................................."

# Root password
echo "root:$rootpw" | chpasswd

# Non-root user
useradd -m $nonrootusername || { echo "FAILED: useradd"; exit 1; }
echo "$nonrootusername:$nonrootpw" | chpasswd
usermod -aG wheel,audio,video,optical,storage $nonrootusername


echo "................................."
echo "SUDO"
echo "................................."

# In sudoers file, append this line (instead of uncommenting it):
#   %wheel ALL=(ALL) ALL
echo '%wheel ALL=(ALL) ALL' | EDITOR='tee --append' visudo


echo "................................."
echo "GRUB"
echo "................................."

case $bootmode in
    bios)
        # BIOS ..............
        # sdX is the drive, not the partition!
        #grub-install --target=i386-pc /dev/sdX
        #grub-mkconfig -o /boot/grub/grub.cfg
        echo "ERROR: Boot mode BIOS is currently not supported!"
        echo "Could not install GRUB."
        ;;
    uefi)
        # EFI ...............
        pacman --noconfirm -S   efibootmgr \
                                dosfstools \
                                os-prober \
                                mtools || {
            echo "FAILED: pacman"; exit 1;
        }
        mkdir /boot/EFI || {
            echo "FAILED: mkdir /boot/EFI";
            exit 1;
        }
        mount "${drivename}1" /boot/EFI || {
            echo "FAILED: mount ... /boot/EFI";
            exit 1;
        }
        grub-install --target=x86_64-efi --bootloader-id=grub_uefi --recheck || {
            echo "FAILED: grub-install"; exit 1;
        }
        grub-mkconfig -o /boot/grub/grub.cfg || {
            echo "FAILED: grub-mkconfig"; exit 1;
        }
        echo "GRUB install successful"
        ;;
    *)
        echo "ERROR: Unknown boot mode: $bootmode"
        echo "Could not install GRUB."
        ;;
esac


echo "................................."
echo "NETWORKING"
echo "................................."

systemctl enable NetworkManager || {
    echo "FAILED: systemctl enable NetworkManager";
    exit 1;
}


echo "................................."
echo "AUR"
echo "................................."

pacman --noconfirm -S base-devel git || {
    echo "FAILED: pacman"; exit 1;
}

# TODO: Make this work without user interaction
cd /home/$nonrootusername
git clone https://aur.archlinux.org/yay-git.git
chown $nonrootusername:$nonrootusername yay-git/
cd yay-git
echo "Building yay as $nonrootusername..."
su $nonrootusername --session-command "makepkg --noconfirm -si" || {
    echo "FAILED: su --session-command makepkg"; exit 1;
}
cd ..
rm -rf yay-git || { echo "FAILED: rm"; exit 1; }


echo "................................."
echo "SETUP"
echo "................................."

git clone https://codeberg.org/REDS1736/sysconfig.git || {
    echo "FAILED: git clone"; exit 1;
}
cd sysconfig
chmod +x setup.sh
chown $nonrootusername:$nonrootusername setup.sh
su $nonrootusername --session-command \
    "sh setup.sh \
        $gitmail \
        $gitname \
        $setupraid \
        $setupsoundcard \
        $setupkeychron" || {
    echo "FAILED: su --session-command sh setup.sh"; exit 1;
}

rm -r sysconfig

exit 0
