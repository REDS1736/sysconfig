#!/bin/bash

domain=$1

timestamp=$(date +"%Y-%m-%d-%H-%M-%S")
backuproot="/run/media/max/OSCHI/backup/dar"

case "$domain" in
    "album")
        echo "-------------------------------------------------------"
        echo "---------------------- ALBUM --------------------------"
        echo "-------------------------------------------------------"
        if compgen -G "$backuproot/album/dar_album*" > /dev/null; then
            echo "... incremental"
            dar -z -R /data/album -A $backuproot/album/dar_album -c "$backuproot/album/dar_album-$timestamp"
        else
            echo "... initial"
            dar -b -z -R /data/album -c $backuproot/album/dar_album
        fi
        ;;
    "audio")
        echo "-------------------------------------------------------"
        echo "---------------------- AUDIO --------------------------"
        echo "-------------------------------------------------------"
        if compgen -G "$backuproot/audio/dar_audio*" > /dev/null; then
            echo "... incremental"
            dar -z -R /data/audio -A $backuproot/audio/dar_audio -c "$backuproot/audio/dar_audio-$timestamp"
        else
            echo "... initial"
            dar -b -z -R /data/audio -c $backuproot/audio/dar_audio
        fi
        ;;
    "creations")
        echo "-------------------------------------------------------"
        echo "--------------------- CREATIONS -----------------------"
        echo "-------------------------------------------------------"
        if compgen -G "$backuproot/creations/dar_creations*" > /dev/null; then
            echo "... incremental"
            dar -z -R /data/creations -A $backuproot/creations/dar_creations -c "$backuproot/creations/dar_creations-$timestamp"
        else
            echo "... initial"
            dar -b -z -R /data/creations -c $backuproot/creations/dar_creations
        fi
        ;;
    "documents")
        echo "-------------------------------------------------------"
        echo "--------------------- DOCUMENTS -----------------------"
        echo "-------------------------------------------------------"
        if compgen -G "$backuproot/documents/dar_documents*" > /dev/null; then
            echo "... incremental"
            dar -z -R /data/documents  -A $backuproot/documents/dar_documents -c "$backuproot/documents/dar_documents-$timestamp"
        else
            echo "... initial"
            dar -b -z -R /data/documents -c $backuproot/documents/dar_documents
        fi
        ;;
    "home")
        echo "-------------------------------------------------------"
        echo "----------------------- HOME --------------------------"
        echo "-------------------------------------------------------"
        if compgen -G "$backuproot/home/dar_home*" > /dev/null; then
            echo "... incremental"
            dar -z -R /home/max \
                -X "*.swp" \
                -X "*.tmp" \
                -X "/home/max/.anaconda" \
                -X "/home/max/.audacity-data" \
                -X "/home/max/.bash_history" \
                -X "/home/max/.cache" \
                -X "/home/max/.cargo" \
                -X "/home/max/.cpan" \
                -X "/home/max/.dvdcss" \
                -X "/home/max/.eclipse" \
                -X "/home/max/.fltk" \
                -X "/home/max/.gimp-2.8" \
                -X "/home/max/.gimp-2.8" \
                -X "/home/max/.git-credentials" \
                -X "/home/max/.gnome" \
                -X "/home/max/.gnupg" \
                -X "/home/max/.gtkrc-2.0" \
                -X "/home/max/.ipynb_checkpoints" \
                -X "/home/max/.lesshst" \
                -X "/home/max/.local/share/Trash/*" \
                -X "/home/max/.mozilla" \
                -X "/home/max/.npm" \
                -X "/home/max/.nv" \
                -X "/home/max/.pgadmin" \
                -X "/home/max/.pki" \
                -X "/home/max/.psql_history" \
                -X "/home/max/.pylint.d" \
                -X "/home/max/.python_history" \
                -X "/home/max/.recently-used" \
                -X "/home/max/.sqlite_history" \
                -X "/home/max/.thumbnails" \
                -X "/home/max/.viminfo" \
                -X "/home/max/.wget-hsts" \
                -X "/home/max/.xonotic" \
                -X "/home/max/.xournal" \
                -X "/home/max/.zcompdump" \
                -X "/home/max/.zhistory" \
                -X "/home/max/.zoom" \
                -X "/home/max/.zotero" \
                -A $backuproot/home/dar_home \
                -c "$backuproot/home/dar_home-$timestamp"
        else
            echo "... initial"
            dar -b -z -R /home/max \
                -X "*.swp" \
                -X "*.tmp" \
                -X "/home/max/.anaconda" \
                -X "/home/max/.audacity-data" \
                -X "/home/max/.bash_history" \
                -X "/home/max/.cache" \
                -X "/home/max/.cargo" \
                -X "/home/max/.cpan" \
                -X "/home/max/.dvdcss" \
                -X "/home/max/.eclipse" \
                -X "/home/max/.fltk" \
                -X "/home/max/.gimp-2.8" \
                -X "/home/max/.gimp-2.8" \
                -X "/home/max/.git-credentials" \
                -X "/home/max/.gnome" \
                -X "/home/max/.gnupg" \
                -X "/home/max/.gtkrc-2.0" \
                -X "/home/max/.ipynb_checkpoints" \
                -X "/home/max/.lesshst" \
                -X "/home/max/.local/share/Trash/*" \
                -X "/home/max/.mozilla" \
                -X "/home/max/.npm" \
                -X "/home/max/.nv" \
                -X "/home/max/.pgadmin" \
                -X "/home/max/.pki" \
                -X "/home/max/.psql_history" \
                -X "/home/max/.pylint.d" \
                -X "/home/max/.python_history" \
                -X "/home/max/.recently-used" \
                -X "/home/max/.sqlite_history" \
                -X "/home/max/.thumbnails" \
                -X "/home/max/.viminfo" \
                -X "/home/max/.wget-hsts" \
                -X "/home/max/.xonotic" \
                -X "/home/max/.xournal" \
                -X "/home/max/.zcompdump" \
                -X "/home/max/.zhistory" \
                -X "/home/max/.zoom" \
                -X "/home/max/.zotero" \
                -c $backuproot/home/dar_home
        fi
        ;;
    "job")
        echo "-------------------------------------------------------"
        echo "------------------------ JOB --------------------------"
        echo "-------------------------------------------------------"
        if compgen -G "$backuproot/job/dar_job*" > /dev/null; then
            echo "... incremental"
            dar -z -R /data/job -A $backuproot/job/dar_job -c "$backuproot/job/dar_job-$timestamp"
        else
            echo "... initial"
            dar -b -z -R /data/job -c $backuproot/job/dar_job
        fi
        ;;
    "sheetmusic")
        echo "-------------------------------------------------------"
        echo "-------------------- SHEETMUSIC -----------------------"
        echo "-------------------------------------------------------"
        if compgen -G "$backuproot/sheetmusic/dar_sheetmusic*" > /dev/null; then
            echo "... incremental"
            dar -z -R /data/sheetmusic -A $backuproot/sheetmusic/dar_sheetmusic -c "$backuproot/sheetmusic/dar_sheetmusic-$timestamp"
        else
            echo "... initial"
            dar -b -z -R /data/sheetmusic -c $backuproot/sheetmusic/dar_sheetmusic
        fi
        ;;
    "uni")
        echo "-------------------------------------------------------"
        echo "------------------------ UNI --------------------------"
        echo "-------------------------------------------------------"
        if compgen -G "$backuproot/uni/dar_uni*" > /dev/null; then
            echo "... incremental"
            dar -z -R /data/uni -A $backuproot/uni/dar_uni -c "$backuproot/uni/dar_uni-$timestamp"
        else
            echo "... initial"
            dar -b -z -R /data/uni -c $backuproot/uni/dar_uni
        fi
        ;;
    *)
        echo "default"
esac


