if [ -d "/run/media/max/OSCHI/backup" ]; then

    echo "ALBUM ================================================"
    ./backup_album.sh
    echo "AUDIO ================================================"
    ./backup_audio.sh
    echo "CREATIONS ============================================"
    ./backup_creations.sh
    echo "DOCUMENTS ============================================"
    ./backup_documents.sh
    echo "HOME ================================================="
    ./backup_home.sh
    echo "JOB =================================================="
    ./backup_job.sh
    echo "SHEETMUSIC ==========================================="
    ./backup_sheetmusic.sh
    echo "UNI =================================================="
    ./backup_uni.sh
    echo " backup done "
else
    echo " no backup drive "
fi

