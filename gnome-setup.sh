# GSETTINGS -------------------------------------------------------------------

gsettings set org.gnome.desktop.interface \
    gtk-theme "WhiteSur-light"
gsettings set org.gnome.desktop.interface \
    icon-theme "Tela"
gsettings set org.gnome.desktop.background \
    picture-uri "file:///usr/share/backgrounds/P1070275_01.jpg"
gsettings set org.gnome.desktop.input-sources \
    xkb-options "['lv3:menu_switch', 'compose:ralt']"
gsettings set org.gnome.desktop.input-sources \
    sources "[('xkb', 'us'), ('xkb', 'de')]"

gsettings set org.gnome.desktop.wm.preferences \
    resize-with-right-button true

gsettings set org.gnome.desktop.wm.keybindings \
    switch-applications "[]"
gsettings set org.gnome.desktop.wm.keybindings \
    switch-windows "['<Alt>Tab']"
gsettings set org.gnome.desktop.wm.keybindings \
    close "['<Shift><Super>q']"


# GNOME-EXTENSIONS ------------------------------------------------------------

# TODO: Not only enable custom shell theme but also set it
gnome-extensions enable \
    user-theme@gnome-shell-extensions.gcampax.github.com
gnome-extensions enable \
    launch-new-instance@gnome-shell-extensions.gcampax.github.com
gnome-extensions enable \
    blur-my-shell@aunetx

