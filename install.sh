#! /bin/sh

pacman-key --populate archlinux || {
    echo "FAILED: pacman-key"; exit 1;
}
pacman --noconfirm -Syy


echo "-----------------------------------------------"
echo "SETTINGS"
echo "-----------------------------------------------"

while :
do
    read -p "Keymap? [de/en] " keymap

    case $keymap in
        de|en)
            break
            ;;
        *)
            echo "Invalid input!"
            echo ""
            ;;
    esac
done

read -p "Hostname: " hostname

while :
do
    read -p "Root password: " rootpw1
    read -p "Confirm:       " rootpw2

    if [ "$rootpw1" = "$rootpw2" ]; then
        echo ""
        break
    else
        echo "Passwords don't match. Try again!"
        echo ""
    fi
done

read -p "Non-root user name: " nonrootusername

while :
do
    read -p "Password: " nonrootpw1
    read -p "Confirm:  " nonrootpw2

    if [ "$rootpw1" = "$rootpw2" ]; then
        echo ""
        break
    else
        echo "Passwords don't match. Try again!"
        echo ""
    fi
done

read -p "Git email address: " gitmail
read -p "Git ident name:    " gitname
echo ""

while :
do
    read -p "Setup RAID? [y/n] " setupraid

    case $setupraid in
        y|Y)
            setupraid="y"
            echo ""
            break
            ;;
        n|N)
            setupraid="n"
            echo ""
            break
            ;;
        *)
            echo "Invalid input!"
            echo ""
            ;;
    esac
done

while :
do
    read -p "Setup external sound card? [y/n] " setupsoundcard

    case $setupsoundcard in
        y|Y)
            setupsoundcard="y"
            echo ""
            break
            ;;
        n|N)
            setupsoundcard="n"
            echo ""
            break
            ;;
        *)
            echo "Invalid input!"
            echo ""
            ;;
    esac
done

while :
do
    read -p "Setup Keychron keyboard? [y/n] " setupkeychron

    case $setupkeychron in
        y|Y)
            setupkeychron="y"
            echo ""
            break
            ;;
        n|N)
            setupkeychron="n"
            echo ""
            break
            ;;
        *)
            echo "Invalid input!"
            echo ""
            ;;
    esac
done


echo "-----------------------------------------------"
echo "BIOS / UEFI"
echo "-----------------------------------------------"

if [ -d "/sys/firmware/efi/efivars" ]; then
    bootmode="uefi"
else
    bootmode="bios"
fi

echo "Boot mode: $bootmode"


echo "-----------------------------------------------"
echo "KEYMAP"
echo "-----------------------------------------------"

case $keymap in
    de)
        loadkeys de-latin1 || { echo "FAILED: loadkeys de-latin1"; exit 1; }
        ;;
    en)
        loadkeys us || { echo "FAILED: loadkeys us"; exit 1; }
        ;;
esac


echo "-----------------------------------------------"
echo "SYSTEM CLOCK"
echo "-----------------------------------------------"

timedatectl set-ntp true || {
    echo "FAILED: timedatectl set-ntp true";
    exit 1;
}
timedatectl status

echo "-----------------------------------------------"
echo "PARTITION DISKS"
echo "-----------------------------------------------"

# BIOS: +550M           BIOS Boot           4
#       (optional) +2G  Linux Swap          19
#       Rest            Linux Filesystem    20
#
# UEFI: +550M           EFI System          1
#       (optional) +2G  Linux Swap          19
#       Rest            Linux Filesystem    20

if fdisk -l | grep "sda"; then
    drivename="/dev/sda"
else
    drivename="/dev/vda"
fi
echo "Formatting $drivename"
read -p "Press Enter to continue"

# TODO: make this work
# https://stackoverflow.com/questions/35166147/bash-script-with-fdisk
case $bootmode in
    bios)
        fdisk "$drivename" <<EEOF
        g
        n


        +550M
        n


        +2G
        n



        t
        1
        4
        t
        2
        19
        t
        3
        20
        w

EEOF
        ;;
    uefi)
        fdisk "$drivename" <<EEOF
        g
        n


        +550M
        n


        +2G
        n



        t
        1
        1
        t
        2
        19
        t
        3
        20
        w

EEOF
        ;;
    *)
        echo "Error! boot mode: $bootmode"
        exit 1
        ;;
esac

echo "Disk partitioning successful"


echo "-----------------------------------------------"
echo "MAKE FILESYSTEMS"
echo "-----------------------------------------------"

# EFI: FAT32
mkfs.fat -F32 "${drivename}1" || { echo "FAILED: mkfs.fat"; exit 1; }

# Swap
mkswap "${drivename}2" || { echo "FAILED: mkswap"; exit 1; }
swapon "${drivename}2" || { echo "FAILED: swapon"; exit 1; }

# Main Filesystem
mkfs.ext4 "${drivename}3" || { echo "FAILED: mkfs.ext4"; exit 1; }


echo "-----------------------------------------------"
echo "INSTALL BASE SYSTEM"
echo "-----------------------------------------------"

mount "${drivename}3" /mnt || {
    echo 'FAILED: mount "${drivename}3"'; exit 1;
}

pacstrap /mnt base linux linux-firmware || {
    echo "FAILED: pacstrap"; exit 1;
}

genfstab -U /mnt >> /mnt/etc/fstab


echo "-----------------------------------------------"
echo "CHROOT"
echo "-----------------------------------------------"

curl https://codeberg.org/REDS1736/sysconfig/raw/branch/master/chroot-script.sh > /mnt/chroot-script.sh
chmod +x /mnt/chroot-script.sh

arch-chroot /mnt ./chroot-script.sh \
    $hostname \
    $rootpw1 \
    $nonrootusername \
    $nonrootpw1 \
    $gitmail \
    $gitname \
    $bootmode \
    $setupraid \
    $setupsoundcard \
    $setupkeychron \
    $drivename

rm /mnt/chroot-script.sh || {
    echo "FAILED: rm /mnt/chroot-script.sh";
    exit 1;
}


exit 0
