# Windows Configuration

## Disable:
- Autostart OneDrive

## Programs:
- 7zip
- Darktable
- DBeaver
- Discord
- Drawboard
- Eduroam
- Firefox, Chrome, Chromium
- geek uninstaller
- GIMP
- git
- JupyterLab
- KeePass
- MiKTeX
- MobileSheets Pro
- Netflix
- Notepad++
- PostgreSQL
- PSPP
- Signal
- Uni-VPN
- Visual Studio Community? ... ChordsDB
- VS Code
- VLC
- WinDirStat
- Zotero

