sudo pacman --noconfirm -S postgres
passwd postgres 1234

exit 0

# One-time setup, not all of it needed in install script
sudo mkdir /etc/systemd/system/postgresql.service.d

echo "[Service]" \
    >> /etc/systemd/system/postgresql.service.d/PGROOT.conf
echo "Environment=PGROOT=/data/dev/postgres/data" \
    >> /etc/systemd/system/postgresql.service.d/PGROOT.conf
echo "PIDFile=/data/dev/postgres/data/postmaster.pid" \
    >> /etc/systemd/system/postgresql.service.d/PGROOT.conf

mkdir /data/dev/postgres
mkdir /data/dev/postgres/data
sudo chown -R postgres:postgres /data/dev/postgres/data

su postgres --session-command \
    "initdb \
        --locale=en_US.UTF-8
        -E UTF8
        -D /data/dev/postgres/data"

sudo systemctl daemon-reload
sudo systemctl start postgresql.service
sudo systemctl enable postgresql.service

su postgres --session-command \
    "createuser --superuser max"

su postgres --session-command \
    "createdb PARI_db1"

sudo pacman --noconfirm -S dbeaver

