#! /bin/sh

gitmail="$1"
gitname="$2"
setupraid="$3"
setupsoundcard="$4"
setupkeychron="$5"

echo "-----------------------------------------------"
echo "IMAGES"
echo "-----------------------------------------------"

# Used as GNOME bg and lightdm-gtk-greeter bg
sudo cp img/P1070275_01.jpg /usr/share/backgrounds/


echo "-----------------------------------------------"
echo "NVIDIA"
echo "-----------------------------------------------"

# TODO: Install proper graphics driver
# Something along the following lines
sudo pacman --noconfirm -S xf86-video-vesa || {
    echo "FAILED: pacman"; exit 1;
}

echo "-----------------------------------------------"
echo "Grub"
echo "-----------------------------------------------"

sudo cp -r sys/grub /etc/default/grub || {
    echo "FAILED: cp ... /etc/default/grub"; exit 1;
}
sudo grub-mkconfig -o /boot/grub/grub.cfg || {
    echo "FAILED: grub-mkconfig"; exit 1;
}


echo "-----------------------------------------------"
echo "STORAGE: fstab, RAID5"
echo "-----------------------------------------------"

case setupraid in
    y)
        echo "Setting up RAID"
        # TODO: Does this work?
        # - Is `mdadm --assemble --scan` even necessary?
        # - Is the array device always located in /dev/md126?
        # - Add to fstab for auto-mounting
        sudo mkdir /data
        sudo pacman --noconfirm -S mdadm || { echo "FAILED: pacman"; exit 1; }
        #sudo mdadm --assemble --scan || { echo "FAILED"; exit 1; }
        sudo mount /dev/md126 /data

        # TODO: Make this work... got some kind of permission problem
        sudo echo ""                                             >> /etc/fstab
        sudo echo "# /dev/md126 - RAID5"                         >> /etc/fstab
        sudo echo \
"UUID=23e66be4-103f-4cab-a8ec-d4abaf58e9f6 \
/data \
ext4 \
defaults,noatime \
0 \
2" >> /etc/fstab
        ;;
    n)
        echo "No RAID setup"
        ;;
    *)
        echo "Invalid value for setupraid: $setupraid"
        read -p "Press Enter to continue"
        ;;
esac


echo "-----------------------------------------------"
echo "ALSA"
echo "-----------------------------------------------"

case $setupsoundcard in
    y)
        echo "Setting up external sound card"
        # TODO: Soundcard setup
        sudo pacman --noconfirm -S alsa-utils || {
            echo "FAILED: pacman"; exit 1;
        }
        sudo cp         sys/asound.conf     /etc/asound.conf
        ;;
    n)
        echo "No external sound card setup"
        ;;
    *)
        echo "Invalid value for setupsoundcard: $setupsoundcard"
        read -p "Press Enter to continue"
        ;;
esac


echo "-----------------------------------------------"
echo "xorg + lightdm"
echo "-----------------------------------------------"

# TODO: Get F keys working

sudo pacman --noconfirm -S  xorg \
                            xorg-xinit \
                            xorg-xrandr \
                            lightdm \
                            lightdm-gtk-greeter || {
    echo "FAILED: pacman"; exit 1;
}

sudo cp     sys/Xsession            /etc/lightdm/Xsession
cp          sys/.Xkbmap             ~/.Xkbmap
cp          sys/.xinitrc            ~/.xinitrc

sudo systemctl enable lightdm || {
    echo "FAILED: systemctl enable lightdm";
    exit 1;
}

case $setupkeychron in
    y)
        echo "Setting up keychron.service"
        sudo cp sys/keychron.service    /etc/systemd/system/keychron.service
        sudo systemctl enable keychron.service || {
            echo "FAILED: systemctl enable keychron.service";
            exit 1;
        }
        ;;
    n)
        echo "No keychron setup"
        ;;
    *)
        echo "Invalid value for setupkeychron: $setupkeychron"
        read -p "Press Enter to continue"
        ;;
esac


echo "-----------------------------------------------"
echo "i3wm"
echo "-----------------------------------------------"

sudo pacman --noconfirm -S  i3-gaps \
                            nitrogen \
                            picom || {
    echo "FAILED: pacman"; exit 1;
}

sudo cp     sys/lightdm.conf                /etc/lightdm/lightdm.conf
sudo cp     sys/lightdm-gtk-greeter.conf    /etc/lightdm/lightdm-gtk-greeter.conf
cp          sys/picom.conf                  ~/.config/picom.conf

# dependencies
# TODO: add `polybar`. Removed it for debugging purposes.
yay --noconfirm -S  nerd-fonts-mononoki \
                    i3exit \
                    i3lock-color || {
    echo "FAILED: yay"; exit 1;
}
sudo pacman  --noconfirm -S dunst \
                            xorg-xev \
                            alacritty \
                            rofi \
                            scrot \
                            flameshot \
                            nemo \
                            xorg-xkill || {
    echo "FAILED: pacman depencencies"; exit 1;
}

cp -r   sys/i3              ~/.config/
chmod +x    ~/.config/i3/scripts/*

cp -r   sys/dunst           ~/.config/

cp -r   sys/polybar         ~/.config/
chmod +x    ~/.config/polybar/scripts/*

cp -r   sys/rofi            ~/.config/

mkdir ~/.config/alacritty
cp      sys/alacritty.yml   ~/.config/alacritty/alacritty.yml

echo "-----------------------------------------------"
echo "GNOME"
echo "-----------------------------------------------"

# Base install
sudo pacman --noconfirm -S  gnome \
                            gnome-software-packagekit-plugin \
                            gnome-tweaks || {
    echo "FAILED: pacman"; exit 1;
}
yay --noconfirm -S  chrome-gnome-shell \
                    gnome-shell-extension-blur-my-shell-git || {
    echo "FAILED: yay"; exit 1;
}
# sudo pacman --noconfirm -Rs gnome-books \
#                             gnome-boxes \
#                             gnome-calculator \
#                             gnome-clocks \
#                             gnome-contacts \
#                             gnome-maps \
#                             gnome-music \
#                             gnome-photos \
#                             gnome-software || {
#     echo "FAILED"; exit 1;
# }

# Customization
yay --noconfirm -S  tela-icon-theme \
                    orchis-theme-git || {
    read -p "FAILED: yay. No reason to exit though";
}
# TODO: WhiteSur install breaks everything
# Maybe install it manually (learn from PKGBUILD)?
#yay --noconfirm -S whitesur-gtk-theme-git

# TODO: Set locale / language (for gnome-terminal to work)

# TODO: Configure `gnome-setup.sh` to be run in first GNOME session


echo "-----------------------------------------------"
echo "Neovim + LaTeX Workflow"
echo "-----------------------------------------------"

sudo pacman --noconfirm -S  python-pip \
                            texlive-most \
                            pandoc \
                            jq || {
    echo "FAILED: pacman"; exit 1;
}
cp -r           sys/nvim    ~/.config/

# Install plugins
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
python -m pip install neovim
python -m pip install pandoc

# TODO: You need to compile YCM before using it
# TODO: Sometimes stays in vim, needs manual intervention (:q:q)
#       Could probably be fixed with:
#       python3 ~/.vim/bundle/YouCompleteMe/install.py
nvim +PluginInstall


echo "-----------------------------------------------"
echo "fish shell"
echo "-----------------------------------------------"

sudo pacman --noconfirm -S  fish \
                            lolcat \
                            exa || {
    echo "FAILED: pacman"; exit 1;
}

cp -r       sys/fish        ~/.config/
cp          sys/.bash_profile   ~/.bash_profile
cp          sys/.bashrc         ~/.bashrc


echo "-----------------------------------------------"
echo "Jupyter"
echo "-----------------------------------------------"

sudo pacman --noconfirm -S  python-ipykernel \
                            jupyterlab || {
    echo "FAILED: pacman"; exit 1;
}

source /data/dev/venv/datascience/bin/activate
ipython kernel install --user --name=datascience

# TODO: JupyterLab customization:
# - Code font (lieber in Firefox einstellen?)
# - vim plugin


echo "-----------------------------------------------"
echo "LibreOffice"
echo "-----------------------------------------------"

sudo pacman --noconfirm -S  libreoffice-still || {
    echo "FAILED: pacman"; exit 1;
}

sudo cp sys/libreoffice-*.desktop   /usr/share/applications/


echo "-----------------------------------------------"
echo "Git"
echo "-----------------------------------------------"

git config --global credential.helper store
git config --global user.email "$gitmail"
git config --global user.name "$gitname"


echo "-----------------------------------------------"
echo "ranger file explorer"
echo "-----------------------------------------------"

sudo pacman --noconfirm -S  ranger || {
    echo "FAILED: pacman -S ranger"; exit 1;
}

cp -r       sys/ranger      ~/.config/


#echo "-----------------------------------------------"
#echo "Games"
#echo "-----------------------------------------------"
#
#sudo pacman --noconfirm -S  0ad || {
#    echo "FAILED: pacman -S 0ad"; exit 1;
#}

# TODO: dotfiles verschieben!


echo "-----------------------------------------------"
echo "Other"
echo "-----------------------------------------------"

sudo pacman --noconfirm -S  chromium \
                            darktable \
                            digikam \
                            firefox \
                            geary \
                            gimp \
                            gst-libav \
                            trash-cli \
                            vlc || {
    echo "FAILED: pacman"; exit 1;
}

# TODO: add `dwarftherapist`, `google-chrome`
yay --noconfirm -S  dwarffortress \
                    keepassxc \
                    zotero || {
    echo "FAILED: yay"; exit 1;
}

# Might fail
yay --noconfirm -S  pspp

# TODO: Configure these programs
