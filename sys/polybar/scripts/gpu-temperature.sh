#!/bin/bash

temp=$(nvidia-settings --query ThermalSensorReading | grep "Attribute 'ThermalSensorReading'" | grep -o "[0-9][0-9]")

echo "GPU: $temp°C"

