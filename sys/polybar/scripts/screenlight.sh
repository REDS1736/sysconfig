#!/bin/bash

brightness=$(xrandr --verbose | grep "Brightness" | cut -c 14-16)

echo "Screen: $brightness"

