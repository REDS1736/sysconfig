#! /bin/sh

if [ "$(amixer get Master | grep "Mono: Playback" | cut -c 40)" == "n" ]; then
    # Sound is on -> turn it off
    amixer set Master off
else
    # Sound is off -> turn it on
    amixer set Master on
    amixer set Headphone on
    amixer set Front on
    amixer set Surround on
    amixer set Center on
    amixer set LFE on
fi
