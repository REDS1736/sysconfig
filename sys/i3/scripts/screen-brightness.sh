#! /bin/sh

#!/bin/bash

step=1

brightness=$(xrandr --verbose | grep "Brightness" | cut -c 14-16)
seconddigit=$(xrandr --verbose | grep "Brightness" | cut -c 16)

newb="$brightness"
case "$brightness" in
    0.0)
        case "$1" in
            up)
                newdigit=$[$seconddigit + $step]
                newb="0.$newdigit"
                ;;
        esac
        ;;
    1.0)
        case "$1" in
            down)
                newdigit=$[10 - 1]
                newb="0.$newdigit"
                ;;
        esac
        ;;
    *)
        case "$1" in
            up)
                case "$seconddigit" in
                    9)
                        newb="1.0"
                        ;;
                    *)
                        newdigit=$[$seconddigit + 1]
                        newb="0.$newdigit"
                        ;;
                esac
                ;;
            down)
                newdigit=$[$seconddigit - 1]
                newb="0.$newdigit"
                ;;
        esac
        ;;
esac

echo "$newb"
xrandr --output HDMI-0 --brightness $newb
