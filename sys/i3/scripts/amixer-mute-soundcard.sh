#! /bin/sh

if [ "$(amixer get Headphone | grep "Front Left:" | rev | cut -c 2)" = "n" ]; then
    # Sound is on -> turn it off
    amixer set Headphone off
    echo "turn sound off"
else
    # Sound is off -> turn it on
    amixer set Headphone on
    echo "turn sound on"
fi
