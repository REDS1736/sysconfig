#! /bin/sh

#!/bin/sh

B='#e9ecef22'  # blank
C='#ced4da'  # clear-ish
D='#e9ecef'  # default
T='#e9ecef'  # text
W='#1b3a51'  # wrong
V='#1b3a51'  # verifying
IMG='/usr/share/backgrounds/blade-runner-2049/BR2049-Wallpaper-Dam.png'

# COLORS + IMAGE ------------------------------------------
case "$1" in
    bladerunner-dam)
        B='#e9ecef22'
        C='#ced4da'
        D='#e9ecef'
        T='#e9ecef'
        W='#1b3a51'
        V='#1b3a51'
        IMG='/usr/share/backgrounds/blade-runner-2049/BR2049-Wallpaper-Dam.png'
        ;;
    blur-light)
        B='#e9ecef22'
        C='#ced4da'
        D='#e9ecef'
        T='#e9ecef'
        W='#E45555'
        V='#E45555'
        ;;
    *)
        echo "== .i3/scripts/lock.sh: invalid / missing argument! =="
        echo "(Default case in colors / image)"
        echo "Valid arguments: bladerunner-dam | blur-light"
        exit 2
esac

# EXEC ----------------------------------------------------
case "$1" in
    bladerunner-dam)  # Image
        i3lock \
        --image "$IMG"          \
        \
        --insidevercolor=$C     \
        --ringvercolor=$V       \
        \
        --insidewrongcolor=$W   \
        --ringwrongcolor=$W     \
        \
        --insidecolor=$B        \
        --ringcolor=$D          \
        --linecolor=$B         \
        --separatorcolor=$D     \
        \
        --verifcolor=$T         \
        --wrongcolor=$T         \
        --timecolor=$T          \
        --datecolor=$T          \
        --layoutcolor=$T        \
        --keyhlcolor=$W         \
        --bshlcolor=$W          \
        \
        --screen 1              \
        --blur 5                \
        --clock                 \
        --indicator             \
        --timestr="%H:%M"    \
        --datestr="%a, %d.%m.%Y"    \
    ;;
    blur-light)       # No image
        i3lock \
        --insidevercolor=$C     \
        --ringvercolor=$V       \
        \
        --insidewrongcolor=$W   \
        --ringwrongcolor=$W     \
        \
        --insidecolor=$B        \
        --ringcolor=$D          \
        --linecolor=$B         \
        --separatorcolor=$D     \
        \
        --verifcolor=$T         \
        --wrongcolor=$T         \
        --timecolor=$T          \
        --datecolor=$T          \
        --layoutcolor=$T        \
        --keyhlcolor=$W         \
        --bshlcolor=$W          \
        \
        --screen 1              \
        --blur 5                \
        --clock                 \
        --indicator             \
        --timestr="%H:%M"    \
        --datestr="%a, %d.%m.%Y"    \
    ;;
    *)
        echo "== .i3/scripts/lock.sh: invalid / missing argument! =="
        echo "(Default case in exec)"
        echo "Valid arguments: | blur-light"
        exit 2
esac
