alias mp3="youtube-dl --extract-audio --audio-format mp3"

alias rm="trash"

alias la="exa --long --git --all"
alias ll="exa --long --git"
alias lt="exa --long --git --tree"

alias jl="jupyter lab"

alias tlmgr="/usr/share/texmf-dist/scripts/texlive/tlmgr.pl --usermode"

alias vim="nvim"

alias config="/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME"

alias findword="grep -rnw './' -e "

alias linevast="sshpass -p b9bwhja1ifhhkg ssh root@83.171.238.254"

alias R="radian"

alias com="sh ~/.config/fish/scripts/gum-commit.sh"

