source ~/.config/fish/functions/zenline.fish
source ~/.config/fish/functions/scan.fish

# Window title
function fish_title
    echo (status current-command) " "
    pwd
end

# Greeting (run when entering interactive mode)
function fish_greeting
    echo "windirstat: ncdu"
    echo "archlinux-java set <version>"
    echo "USB fooswitch: scythe -1 -m win -a u"
    echo "  https://github.com/rgerganov/footswitch"
    duf -hide special
    sensors | grep --color=never Core
    zenline
end

# Custom prompt
function fish_prompt -d "Write out the prompt"
    set prompt (printf '%s%s@%s%s %s%s' (set_color $fish_color_normal) (whoami) (set_color $fish_color_hostname) (cat /etc/hostname) (set_color $fish_color_cwd) (pwd))
    echo $prompt
    echo "> "
end

fish_vi_key_bindings

bind \
    --mode insert \
    --sets-mode default \
    "jk" repaint

bind \
    --mode insert \
    \cL complete

set -Ux FG_ROOT /data/nerd/fgfs/fgdata

source ~/.config/fish/aliases.fish
