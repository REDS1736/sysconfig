function scan \
    -d "Scan using `scanimage`. Usage: `scan <file name> [pages=1]`"

    # Set number of pages .................................
    switch (count $argv)
        case 0
            echo "Invalid number of arguments!"
            echo "Usage: `scan <file name> [pages=1]`"
        case 1
            set pages 1
        case 2
            set pages $argv[2]
        case '*'
            echo "Invalid number of arguments!"
            echo "Usage: `scan <format> <file name> [pages=1]`"
    end
    echo "pages: $pages"

    # Scan to PNG .........................................
    for i in (seq 1 $pages)
        read -P "Press [Enter] to start scanning page number $i"
        echo "scanning ..."
        scanimage \
            --device "airscan:w0:HP DeskJet 3600 series" \
            --format=png \
            --output-file "scan-temp-$i.png" \
            --progress
    end

    # Convert to PDF ......................................
    echo "converting ..."
    magick scan-temp-*.png $argv[1]
    rm scan-temp-*.png

    echo "Scan ist fertig! Deine Datei heißt $argv[1]" | cowsay
end
