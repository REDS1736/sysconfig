function rcp \
    -d "Upload via rsync to StorageBox. Usage: `rcp <file name> <target dfirectory>`"

    # Set number of pages .................................
    switch (count $argv)
        case 0
            echo "Invalid number of arguments!"
            echo "Usage: `rcp <file name> <target directory>`"
        case 1
            echo "Invalid number of arguments!"
            echo "Usage: `rcp <file name> <target directory>`"
        case 2
            set LOCAL_PATH $argv[1]
            set SERVER_PATH $argv[2]
        case '*'
            echo "Invalid number of arguments!"
            echo "Usage: `rcp <file name> <target directory>`"
    end
    echo "upload $LOCAL_PATH to StorageBox://$SERVER_PATH"

    rsync -e 'ssh -p 23' \
        --human-readable \
        --info=progress2 \
        --partial \
        --progress \
        --recursive \
        --verbose \
        --xattrs \
        "$LOCAL_PATH" \
        "u305269@u305269.your-storagebox.de:$SERVER_PATH"
end
