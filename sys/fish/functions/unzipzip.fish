function unzipzip \

    echo ">> UNZIP"
    unzip $argv[1]/\*.zip

    for d in */
        cd $d
        set wd (pwd)
        echo ">> MOVE: $wd"
        unzipzip $wd
        cd ..
    end

end
