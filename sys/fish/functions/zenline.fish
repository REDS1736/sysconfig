function zenline
    set zen (cat ~/.config/fish/zen.txt)
    set length (count $zen)
    set index (random 1 $length)
    echo "$zen[$index]" | lolcat --freq 0.2
end