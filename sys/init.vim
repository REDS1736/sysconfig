set encoding=utf-8
" VUNDLE ------------------------------------------------------------
set nocompatible
filetype off
set shell=/bin/bash
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" let Vundle manage Vundle
Plugin 'VundleVim/Vundle.vim'
Plugin 'airblade/vim-gitgutter'
Plugin 'lervag/vimtex'
Plugin 'vim-pandoc/vim-pandoc-syntax'
Plugin 'vim-pandoc/vim-pandoc'
Plugin 'preservim/nerdcommenter'
Plugin 'scrooloose/nerdtree'
Plugin 'tpope/vim-surround'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'vim-syntastic/syntastic'
Plugin 'ycm-core/YouCompleteMe'
Plugin 'Yggdroot/indentLine'
Plugin 'sbdchd/neoformat'
call vundle#end()
filetype plugin indent on

" PLUGIN SETTINGS ---------------------------------------------------
"
" Airline ...................
let g:airline_theme='cool'

" Git Gutter ................
set updatetime=100 " Also affects the interval of writing swap files... but idc
highlight SignColumn        ctermbg=black
highlight GitGutterAdd      ctermfg=lightgreen
highlight GitGutterChange   ctermfg=blue
highlight GitGutterDelete   ctermfg=red

" Indent Line ...............
"let g:indentLine_char_list = ['|', '.', '|', '.', '|', '.']
let g:indentLine_char_list = ['|']
" fix missing quotes in JSON
let g:vim_json_conceal=0
let g:vim_json_syntax_conceal=0
let g:indentLine_fileTypeExclude = ['json']

" NERDCommenter .............
noremap <C-c> :call nerdcommenter#Comment(0, "toggle")<CR>
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 0
" Align line-wise comment delimiters flush left instead of following
" code indentation
let g:NERDDefaultAlign = 'left'
" Allow commenting / inverting empty lines
" (Useful for commenting regions)
let g:NERDCommentEmptyLines = 1
" Check for all selected lines if they are commented or not
let g:NERDToggleCheckAllLines = 1

" NERDTree ..................
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>
let NERDTreeMapOpenRecursively='l'
let NERDTreeMapCloseDir='h'
let NERDTreeMapOpenSplit='l'
let NERDTreeMapToggleHidden='H'

" Pandoc ....................
let g:pandoc#filetypes#handled = ["pandoc", "markdown", "latex"]
let g:pandoc#filetypes#pandoc_markdown = 0
let g:pandoc#modules#enabled = ["command"]
fu! LatexToWord()
    :Pandoc docx
    :endfunction
nnoremap <C-l> :call LatexToWord() <CR>

" Syntastic .................
let g:syntastic_python_checkers = ['pylint']
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_auto_loc_list = 2

" YouCompleteMe .............
" Start suggesting after 1 char typed
let g:ycm_min_num_of_chars_for_completion = 1
" Disable documentation popup
let g:ycm_auto_hover = ''
" Go to function definition
let g:jedi#goto_command = "<C-b>"

" VimTeX ....................
fu! LatexCompile()
    :VimtexCompile
    :endfunction
nnoremap <C-p> :call LatexCompile() <CR>
set conceallevel=0
" Disable documentation preview window
" Alternatively type :pc to close preview window
"set completeopt-=preview
"let g:ycm_min_num_of_chars_for_completion = 10
"let g:ycm_max_num_candidates_to_detail = 99
"set completeopt-=popup
"let g:ycm_auto_trigger disabled

" GENERAL -----------------------------------------------------------
" Fix Ctrl-Arrow keys in terminal
map <ESC>[1;5C <C-Right>
map <ESC>[1;5D <C-Left>
map <ESC>[1;5A <C-Up>
map <ESC>[1;5B <C-Down>
" Fix Ctrl-Arrow in alacritty
map <ESC>[C <C-Right>
map <ESC>[D <C-Left>
map <ESC>[A <C-Up>
map <ESC>[B <C-Down>
syntax on
set wrap  " as opposed to set nowrap
set noerrorbells
set number
set smartindent
set incsearch  " start searching as soon as i type
" Whitespace
highlight BadWhitespace ctermbg=yellow
match BadWhitespace /\s\+$/
" Selection
highlight Visual ctermbg=yellow ctermfg=black

" FORMATTING --------------------------------------------------------
" Enable folding, f = folding
set foldmethod=indent
set foldlevel=99
" highlight Folded ctermbg=blue ctermfg=black
" Indent
set tabstop=4
set shiftwidth=4
set expandtab  "Tabs -> Spaces
set linebreak  "Do not break words when wrapping

" INTERACTION -------------------------------------------------------

" Folding
nnoremap f za

" Terminal
function! CreateTerm()
    terminal
    wincmd x
    res 40
    wincmd w
endfunction
nnoremap <C-h> :call CreateTerm()<CR>
inoremap jk <Esc>
vnoremap hl <Esc>

" Auto-center current line
nnoremap <C-k> kzz
nnoremap <C-j> jzz
vnoremap <C-k> kzz
vnoremap <C-j> jzz

" Spell checking
nnoremap zj ]szz
nnoremap zk [szz
" https://www.linux.com/training-tutorials/using-spell-checking-vim/

" Auto-format JSON file
" Calls `jq` which needs to be installed via `pacman -S jq`
function! Json()
    :%!jq .
endfunction

" FILE SPECIFIC STUFF -----------------------------------------------
" PEP 8 formatting
au BufNewFile,BufRead *.py
    \ set colorcolumn=81 |
    \ highlight ColorColumn ctermbg=blue |
    \ set fileformat=unix
" JSON
au BufNewFile,BufRead *.json
    \ :command Json :call Json()
" LaTeX
au BufNewFile,BufRead *.tex
    \ set colorcolumn=81 |
    \ highlight ColorColumn ctermbg=blue |
    \ set fileformat=unix |
    \ set spell spelllang=de
" PSPP
au BufNewFile,BufRead *.sps
    \ set colorcolumn=81 |
    \ highlight ColorColumn ctermbg=blue |
    \ set fileformat=unix
" GTK UI
au BufNewFile,BufRead *.ui
    \ set shiftwidth=2 |
    \ set tabstop=2 |
    \ set fileformat=unix
au BufNewFile,BufRead *.c
    \ set shiftwidth=2 |
    \ set tabstop=2 |
    \ set fileformat=unix
au BufNewFile,BufRead *.md
    \ set colorcolumn=81 |
    \ highlight ColorColumn ctermbg=blue |
    \ set fileformat=unix
" STATUS BAR --------------------------------------------------------
" Show status bar?
set laststatus=0  " 0=never, 2=always
"set statusline=helloworld
" Assemble Statusline
"set statusline=
"set statusline+=%#PmenuSel#
"set statusline+=%#LineNr#
"set statusline+=\ %f        " Filename
"set statusline+=%=          " Align right
"set statusline+=\ %y        " Language
"set statusline+=\ %{&fileencoding?&fileencoding:&encoding}  " Encoding
"set statusline+=\[%{&fileformat}\]
"set statusline+=\ %p%%      " Vertical position in percent
"set statusline+=\ %l:%c     " Line + column
