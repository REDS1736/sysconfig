# Arch Linux Install

1. You run `install.sh`.
2. It calls `chroot-script.sh` from a chroot environment.
3. Inside of the chroot environment, `setup.sh` is called.

## ToDo

- Proper nvidia setup (including `nvidia-settings`) in `setup.sh`
- Get F keys on keychron working
- `chroot-script.sh`: Enable grub setup for BIOS
- WebDAV Setup for mailbox.org via nautilus (or other File managers?)
- Make RAID5 setup in `setup.sh` optional
- GNOME Shell theme
- lightdm background
- RAID5 in fstab for auto-mounting
- GNOME: display all Window buttons (currently only close button)
- Audio setup still needs one-time manual intervention in alsamixer
